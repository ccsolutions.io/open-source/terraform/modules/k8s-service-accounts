resource "kubernetes_namespace_v1" "this" {
  metadata {
    name = var.namespace
  }

  lifecycle {
    ignore_changes = [
      metadata[0].annotations,
      metadata[0].labels
    ]
  }
}
resource "kubernetes_service_account_v1" "this" {
  metadata {
    name      = var.kubernetes_service_account_name
    namespace = var.namespace
  }
  secret {
    name = var.kubernetes_service_account_name
  }
}
resource "kubernetes_secret_v1" "this" {
  metadata {
    name      = var.kubernetes_service_account_name
    namespace = kubernetes_namespace_v1.this.metadata.0.name
    annotations = {
      "kubernetes.io/service-account.name" = "${var.kubernetes_service_account_name}"
    }
  }
  type = "kubernetes.io/service-account-token"
}
resource "kubernetes_cluster_role_binding" "this" {
  metadata {
    name = var.kubernetes_service_account_name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.this.metadata.0.name
    namespace = kubernetes_namespace_v1.this.metadata.0.name
  }
}
resource "local_sensitive_file" "this" {
  filename = "${path.root}/outputs/serviceAccount_${var.kubernetes_service_account_name}.yaml"
  content = templatefile("${path.module}/templates/kubeconfig.yaml.tmpl",
    {
      cluster_ca                      = base64encode(var.cluster_ca)
      cluster_endpoint                = var.cluster_endpoint
      cluster_name                    = var.cluster_name
      namespace                       = var.namespace
      kubernetes_service_account_name = var.kubernetes_service_account_name
      kubernetes_token                = kubernetes_secret_v1.this.data.token
  })
}
