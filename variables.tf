variable "kubernetes_service_account_name" {
  description = "The name of the service account"
  type        = string
  default     = "sa-example"
}
variable "namespace" {
  description = "The namespace in which the service account should be created"
  type        = string
  default     = "my-namespace"
}
variable "cluster_ca" {
  description = "The CA of the Kubernetes cluster"
  type        = string
}
variable "cluster_endpoint" {
  description = "The endpoint of the Kubernetes cluster"
  type        = string
}
variable "cluster_name" {
  description = "The name of the Kubernetes cluster"
  type        = string
}
