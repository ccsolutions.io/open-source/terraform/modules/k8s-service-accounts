# k8s-service-accounts

## Usage
This module creates a Kubernetes service account, a secret, a binded role cluster and generate the kubeconfig for the users.

```hcl
module "k8s-service-accounts" {
  source  = "ccsolutions.io/modules/k8s-service-accounts"
  version = "1.0.2"

  kubernetes_service_account_name = "my-username"
  namespace                       = "user-my-username-namespace"
  cluster_ca                      = "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUMrVENDQWVHZ0F3SUJBZ0lKQUtQcmRGenlkZCs4TUEwR0NTcUdTSWIzRFFFQkN3VUFNQkl4RURBT0JnTlYKQkFNTUIydDFZbVV0WTJFd0lCY05NVGd4TVRBMk1UQXpPVEUzV2hnUE1qRXhPREV3TVRNeE1ETTVNVGRhTUJJeApFREFPQmdOVkJBTU1CMnQxWW1VdFkyRXdnZ0VpTUEwR0NTcUdTSWIzRFFFQkFRVUFBNElCRHdBd2dnRUtBb0lCCkFRRFU5ZlQ0YTJFTnFGVmljZEs0dnVUV1hJMnMyR3h2emhCV3d6TjBtMlMrdXZaREh1SG9KdEJaOEZqaEF2M1oKalJSSEJWamEvMnJEeVVtcUlpN1UzTjNNTmNoSzY1eXJvM2JrUm5UcHRnVnNBZUphMmVnc08waFFnSWk1QnhYYgpWcUNPTWpQOWM1cDlMa3lSdjZQeGxqT0p3bGg5VTFVT1RzNUZpMlpQditlNkRzd0t2VE1wOW43bEp4ZzNXVjVvCjl5N2IrTjYvRDkyTEpMeGUvaFJ2bXdsQzRDMVRGMlpyUGlqMWkzdjRER0hJK1pMeE8zbnZkTm5USnN4dEFvMFgKRmFGSE4yelJYNmhSSXU2OEIrakxTRVhyZjJUbHRGQ0ViRWJGZC9UaU0vNjdxSUZNazVseG1zZ2s5VFFldnhVcgpLTG1vNFdodXkvYTN6MHJvc3RrNHp3NlZBZ01CQUFHalVEQk9NQjBHQTFVZERnUVdCQlQ3eEVjNFNhTFo1c2VZCm0vQTBPaFlHazB3VVpqQWZCZ05WSFNNRUdEQVdnQlQ3eEVjNFNhTFo1c2VZbS9BME9oWUdrMHdVWmpBTUJnTlYKSFJNRUJUQURBUUgvTUEwR0NTcUdTSWIzRFFFQkN3VUFBNElCQVFBdW1lbU15N2dMV2pVVDd6T1BlNXZLcWlVegpsd2tMdmZUOWRLcHppV0FXeXdmMzRFbVpiN1VuVnJjRjE1NENyamp5aWxJdjJmaUJQRmdRZUkvNlk3MlZHZzIrCkhPWTV2d1lWRFArRFlOb0xDdU4wRmZzeEpRbVJJYXRDVGJ6bitNRmUwWURtVXpjVW9tbk9vTHdnSG9IT2o2VTEKMkpwbjdBTndmMXBXbEdVVjlhejFOYnZOclFPZE5WbWJkYzRlWlQ3N2NQYTFYSWhIRW5EbkJydGdGZjdNYzBKTgppWnNBL2ZhbHJKWTFsb2tWYzdwZXNvazdyRXBqMmtzZUZyUE5WOTFheWN2SEllYTdad0xwelZ1aktralNnZlFmCkpMVWJ5czZ0Szk5bURsMkxOc1o2QkZSYVloUnRhbjZSQitFbG03dmczcW5PbFJIa1FKbndtRmVrd1J6NgotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg=="
  cluster_endpoint                = "https://cluster.ccsolutions.dev:9443"
  cluster_name                    = "k8s-cluster-fra1"
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.5.3 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | 2.23.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | 2.23.0 |
| <a name="provider_local"></a> [local](#provider\_local) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [kubernetes_cluster_role_binding.this](https://registry.terraform.io/providers/hashicorp/kubernetes/2.23.0/docs/resources/cluster_role_binding) | resource |
| [kubernetes_namespace_v1.this](https://registry.terraform.io/providers/hashicorp/kubernetes/2.23.0/docs/resources/namespace_v1) | resource |
| [kubernetes_secret_v1.this](https://registry.terraform.io/providers/hashicorp/kubernetes/2.23.0/docs/resources/secret_v1) | resource |
| [kubernetes_service_account_v1.this](https://registry.terraform.io/providers/hashicorp/kubernetes/2.23.0/docs/resources/service_account_v1) | resource |
| [local_sensitive_file.this](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/sensitive_file) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cluster_ca"></a> [cluster\_ca](#input\_cluster\_ca) | The CA of the Kubernetes cluster | `string` | n/a | yes |
| <a name="input_cluster_endpoint"></a> [cluster\_endpoint](#input\_cluster\_endpoint) | The endpoint of the Kubernetes cluster | `string` | n/a | yes |
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | The name of the Kubernetes cluster | `string` | n/a | yes |
| <a name="input_kubernetes_service_account_name"></a> [kubernetes\_service\_account\_name](#input\_kubernetes\_service\_account\_name) | The name of the service account | `string` | `"sa-example"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | The namespace in which the service account should be created | `string` | `"my-namespace"` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
