# Runbook for k8s-service-accounts

## Description

This runbook describes the steps required to use the `k8s-service-accounts` module.

## Prerequisites

- Terraform v1.5.1 or higher must be installed.
- A Kubernetes cluster must be available and kubectl must be configured.

## Steps

1. Configure the input variables in the `main.tf` file:

```hcl
module "k8s-service-accounts" {
  source  = "ccsolutions.io/modules/k8s-service-accounts"
  version = "1.0.1"

  kubernetes_service_account_name = "my-service-account"
  namespace = "my-namespace"
}
```

2. Initialize the Terraform project:

```
terraform init
```

3. Plan the changes:

```
terraform plan
```

4. Apply the changes:

```
terraform apply
```

## Verification

- Verify that the service account, secret, and cluster role binding were successfully created:

```
kubectl get serviceaccounts -n my-namespace
kubectl get secrets -n my-namespace
kubectl get clusterrolebindings
```